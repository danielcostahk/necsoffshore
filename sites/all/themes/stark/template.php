<?php
/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to STARTERKIT_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: STARTERKIT_breadcrumb()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */
 
 
 
 
 
 
 
/*-----------------------------------------
	Stark Implementations
-------------------------------------------*/

if(arg(0)!="admin"){ drupal_add_js ('$(document.body).ready(function(){$("#admin-menu").css("display","none");}); ','inline');} 	
 
 
 


/**
 * Implementation of HOOK_theme().
 */
function stark_theme(&$existing, $type, $theme, $path) {


  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_page(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');

  // To remove a class from $classes_array, use array_diff().
  //$vars['classes_array'] = array_diff($vars['classes_array'], array('class-to-remove'));
}
// */



/**
 * Format the "Submitted by username on date/time" for each node
 *
 * @ingroup themeable
 */
function stark_node_submitted($node) {


	/*
  return t('!username on @datetime',
    array(
      '!username' => theme('username', $node),
      '@datetime' => format_date($node->created),
    ));
    */
}



function stark_preprocess_page(&$vars, $hook) {

	//global $language;

	global $user;
	
	//dpm($vars);
	//dpm($hook);

		
	if($user->uid!=1) unset($vars['messages']);
	

	$arg_node = node_load(arg(1));
	$argument = strtolower(arg(0));

	if($arg_node->nid==73||$arg_node->nid==100) {
		$vars['template_files'][] = 'page-popup_page';

	}
	

	if($argument!="forum" && node_load(arg(1))->type !== "forum" && $argument!=="comment" ){
		unset($vars['breadcrumb']);
	}

}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */

function stark_preprocess_node(&$vars, $hook) {
  //$vars['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  	/*
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars, $hook);
  }*/
  
  
  
  
}





// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function STARTERKIT_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */

function stark_preprocess_block(&$vars, $hook) {

	
	
	//dpm($hook);
	
	$block_name = $vars['template_files'][2];
	
	//print "here".$block_name;
	switch ($block_name){
	
		
		case 'block-menu-primary-links':
			unset($vars['block']->subject);
		break;
		
		case 'block-lang_dropdown-0':
			unset($vars['block']->subject);
			//dpm($vars['content']);
		break;
		
		/*
		case 'block-aggregator-feed-3':
		
		
		print_r($vars);
		print "jere".$vars['title'];
		unset($vars['title']);
		*/
		/*
		$vars['title']="Market title";
		
		return $vars;
	
		//unset($vars['block']->subject);
		//$vars['block']->subject="";
		break;
		*/
	}
	
	
	

	 //="nno";
	//print $vars['block']->subject; //= "NONNO";
	
  
  
	  //$vars['block']->subject= implode($subject);
	  //$vars['block']->subject = drupal_render($vars['block']);
	  
	  //print_r($vars); 	
	  /*
	  if(arg(0)!="admin") {
		print_r($vars['block']);
			
			foreach($vars['block'] as $block){
			
				print "here".$block[0]->bid;
			}
			
			/*
			if($vars['block']->bid==intval(29)){
				print "do something here";
				$vars['block'][
				 $variables['search_form'] = implode($variables['search']);
				}

	  }  //arg 0 admin*/

}



function hook_preprocess(&$variables, $hook) {

	//print $hook;

}


/*function stark_user_profile(){

	//print "hello";
	
}*/


function hook_menu_alter(&$items) {
  // Example - disable the page at node/add
 // $items['node/add']['access callback'] = FALSE;
	//print "template";
}


	/**
* Override or insert PHPTemplate variables into the search_theme_form template.
*
* @param $vars
*   A sequential array of variables to pass to the theme template.
* @param $hook
*   The name of the theme function being called (not used in this case.)
*/
function stark_preprocess_search_block_form(&$vars, $hook) {
	//global $user;
	
   // Modify elements of the search form
  //$vars['form']['search_block_form']['#title'] = t('Search mysite.com');
/*
  // Set a default value for the search box
  $vars['form']['search_block_form']['#value'] = t('Search');

  // Add a custom class to the search box
 // $vars['form']['search_block_form']['#attributes'] = array('class' => t('cleardefault'));

  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('');

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_block_form']['#printed']);
  $vars['search']['search_block_form'] = drupal_render($vars['form']['search_block_form']);

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
*/
}













