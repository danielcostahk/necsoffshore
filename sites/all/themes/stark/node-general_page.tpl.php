<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $submitted: Themed submission information output from
 *   theme_node_submitted().
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $build_mode: Build mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $build_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * The following variable is deprecated and will be removed in Drupal 7:
 * - $picture: This variable has been renamed $user_picture in Drupal 7.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess()
 * @see zen_preprocess_node()
 * @see zen_process()
 */
 
// GET VARS FOR GENERAL PAGE

global $language;

$language_index =0;
 if($language->language=='zh-hans') $language_index=1;


$banner_image = $node->field_gp_banner_image[0]['filepath'];


if($main_title = $node->field_gp_main_title[$language_index]['value']) {
$main_image = $node->field_gp_main_image[0]['filepath'];
$main_text = $node->field_gp_maintext[$language_index]['value'];
}

if($sec_title = $node->field_gp_sec_title[$language_index]['value']) {
$sec_image = $node->field_gp_sec_image[0]['filepath'];
$sec_text = $node->field_gp_sec_text[$language_index]['value'];
}


if($third_title = $node->field_gp_third_title[$language_index]['value']) {
$third_image = $node->field_gp_third_image[0]['filepath'];
$third_text = $node->field_gp_third_text[$language_index]['value'];
}

 
$text_limit = 952;
 
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix">
 
 	<div class="main-container">
 	<div class="banner-image"><? print theme_image($banner_image);  ?></div>
    
    <div class="main-content">
    
    	<!-- FIRST SECTION ------------------------------------------->
          <? if($main_title) { ?>
    	<div id="row-1" class="row">
        	<? if(!$main_image) $full_width = "full-width"; ?>
        	
            <div class="left <? print $full_width; ?>">
            
            
            	<h2><? print $main_title; ?></h2>
            	<div class="text-up"><? print $main_text; ?></div>
             	<span class="slidedown-button" style="display:none;"></span>
            
            
            </div>
            
            <div class="right">
           <? print theme_image($main_image);  ?>
           </div>
        </div> <!-- row 1 ends here --->
        <? } ?>
        
        
        
        <!-- Second  SECTION ------------------------------------------->
        <? if($sec_title) { ?>
    	<div id="row-2" class="row">
        	<div class="left">
            <? print theme_image($sec_image);  ?>
          
            </div>
            
            <div class="right">
              
              <h2><? print $sec_title; ?></h2>
              <div class="text-up"><? print $sec_text; ?></div>
              <span class="slidedown-button" style="display:none;"></span>
            	
                
                
           </div>
        </div> <!-- row 1 ends here --->
         <? } ?>
         
         
          <!-- Third  SECTION ------------------------------------------->
        <? if($third_title) { ?>
    	<div id="row-3" class="row">
        	<div class="left">
           
           		<h2><? print $third_title; ?></h2>
                <div class="text-up"><? print $third_text; ?></div>
              	<span class="slidedown-button" style="display:none;"></span>
            		
          
            </div>
            
            <div class="right">
              <? print theme_image($third_image);  ?>
            
           </div>
        </div> <!-- row 1 ends here --->
         <? } ?>
         
    
    	
    </div>
    
    
    </div><!-- Main Container ends here -->
 <?
    
    //print theme_image($banner_image;
	
	
	
 
 
   //print_r($node); ?>
 
</div>  <!-- /.node -->
