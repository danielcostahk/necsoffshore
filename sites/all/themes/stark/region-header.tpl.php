<?php
/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see zen_preprocess()
 * @see zen_preprocess_region()
 * @see zen_process()
 */
?>
<div id="block-header" class="<?php print $classes; ?>">

	
	<? 
		global $base_url;
		global $base_path; 
		$logo_url = drupal_get_path('theme', 'stark')."/stark_images/necs_logo.png";
	
	?>
	
    <div class="header-logo">
         <a href="<? print $base_url."/"; ?>" title="<?php print t('Home'); ?>" rel="home">
            <span class="title"><? print theme_image($logo_url,'Necs Logo','Front Page',NULL, true); ?></span>
          </a>
        </div> 
      <div class="header-top">
		<div class="user-section">
    	<? if($user->uid) { ?>
      	 	<a href="<? print $base_url."/logout"; ?>"><? print t("Logout"); ?></a>  
            <span>|</span>
        	<a href="<? print $base_url."/user"; ?>"><? print t("My Profile"); ?></a>  
        <? } else { ?>
        	<a href="<? print $base_url."/user"; ?>"><? print t("Login"); ?></a> 
             <span>|</span> 
    		<a href="<? print $base_url."/user/register"; ?>"><? print t("Register"); ?></a>  
        <? } ?>
        </div>
    
    </div>  
    <div class="header-bottom">
       
    
      <? /*  
    <div class="main-menu">

    
		//$block = module_invoke('primary-links', 'block', 'view', 0);
		
		$block = module_invoke('primary-links', 'block');
		print_r($block);
			print $block['content'];
		
		
		
		//print theme('links', menu_navigation_links('primary-links')); //no language included
		
		
		/* $menu_array = necs_get_menu_tree('primary-links');
	
	  
             $mid=0;
             foreach ($menu_array as $menu_item) {
                
                if($mid!=0) print '<span class="footer-border"></span>';
                ?>
                
                <span id="<?print "menu-".$mid; ?>" class="container">
                    <span class="menu-title">
                    	<a href="<? print url($menu_children['link_path']); ?>">
                        	<span><? print htmlspecialchars(t($menu_item['link_title'])); ?></span>
                        </a>    
                      </span>
                  <? if($menu_item['children']) { ?>
                   <ul id="footer-menu" class="footer-media-section">
                      
					  		<? foreach ($menu_item['children'] as $menu_children) { ?>
                       <li><a href="<? print url($menu_children['link_path']); ?>"><?php print $menu_children['link_title']; ?></a></li>
                       <? }  ?>
					 
                   </ul>
                  
                   <?  }  ?>
                  </span>
              <? 
              $mid++;
              } 
			 
	   
    	
    
    	</div>   */
			  ?>
    
    
  
    
   <? print $content; ?>

    </div>
  
    

</div><!-- /.region -->
