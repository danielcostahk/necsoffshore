<?
/*----------------------------------------------------------------------------------------

	Name:	jobpost-slider.tpl.php
	
	Description: Job Post Slider
	
	Function: Shows the job post in a slider like manner
	
	$variables : stores the data
	
----------------------------------------------------------------------------------------*/	
	

	// get given variables
	global $base_url;
	global $user;
	
	$register = url('user/register');
	
	
	$jobposts = $variables;
	$quantity = sizeof($jobposts);
	
	//print_r($jobposts);
	
	if($quantity <= 7) {
		$ul_class = 'nocarousel-container';
		$add_class='nocarousel-item';
		}

	
	?>
    
    <div id="jobpost-container">
    
    	<span class="icon"></span><div class="right-title"><h2>Our Last Job Offers</h2></div>
        <? if($quantity ==0 ) {?>
        <div class="no-jobs">
        	<span>Currently there are no jobposts available.</span>
        </div>
        <? } ?>
        <ul class="mycarousel jcarousel-skin-default <? print $ul_class; ?> ">
    	<? for($i=0;$i<sizeof($jobposts);$i++) {  
			$job_post = $jobposts[$i];
			$job_post_link = url('JobPosts/'.$job_post->nid);
			
			$main_title = $job_post->title;
			if(strlen($main_title)>16) $main_title = substr($main_title,0,16).".R.";
			else $main_title = $main_title." >";
			?>
                <li class="<? print $add_class; ?>">R
                <div class="jobpost">
                    <div class="title"><? print $main_title; ?></div>
					<span class="location"><span class="subtitle">Loc:</span>
                    <? 	$location_length = strlen($job_post->field_jobpost_location_value);
						$location_length >13? print substr($job_post->field_jobpost_location_value,0,13)."..":print $job_post->field_jobpost_location_value; ?>
						
                     </span>
                    <span class="experience"><? print "Exp: ".$job_post->field_jobpost_experience_value; ?></span>
                    <span class="published"><? print "Pbl Date: ".date('d-m-y',$job_post->created); ?></span>
                    
                    <div class="application">
                    	
                    	<? if(!$user->uid) { ?>
                        <a class="apply-icon" href="<? print $register; ?>"><? print t('apply'); ?></a>
                    	<? }   else  { ?>
                        <a class="apply-icon" href="<? print $job_post_link; ?>"><? print t('apply'); ?></a>
                        <? } ?>
                      
                        
                        <? print job_post_email_share($job_post->nid,$job_post->title,'frontpage'); ?>
                        </a>
                       
                    </div> 
                </div>
                </li>

    	<? } ?>
         </ul>
    </div>

  <?php
  
  	if($quantity>7)
    jcarousel_add('mycarousel', array('horizontal' => TRUE, 'wrap'=>'circular','visible'=>7,'animation'=>'slow','scroll'=>1 ));
	//'auto'=>0.01
	//'animation'=> 10000
	//
	
  ?>
	

	

	
