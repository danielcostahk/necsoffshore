<?
/*----------------------------------------------------------------------------------------

	Name:	frontpage-slideshow.tpl.php
	
	Description: Custom Template
	
	Function: Render html given a specific data
	
	$variables : stores the data
	
----------------------------------------------------------------------------------------*/	
	

	// get given variables
	$frontpage_node = $variables[0];
	$index=0;
	$intro_title = $frontpage_node->field_frontpage_introtext_title[0]['value'];
	$intro_text=$frontpage_node->field_frontpage_introtext[0]['value'];
	
?>

	<? /*-----SLIDESHOW ----------------------------------------------*/ ?>
    <div class="slideshow-back-strip"></div>
	<div class='slideshow-container'>
    	
		<div class='slideshow'>
        <?  foreach($frontpage_node->field_frontpage_slideshow as $image ){ 
				if($image) { //check if its not null
				$label = $frontpage_node->field_frontpage_slideshow_label[$index]['value'];
				$label_title = $frontpage_node->field_frontpage_slideshow_title[$index]['value'];
				$label_color = $frontpage_node->field_frontpage_slideshow_color[$index]['value'];
				if($label_color) $sc = necs_html2rgb($label_color); else $sc ="";

				?>
				<div class="image-container">
                	<? print theme_image($image["filepath"],$image['title'],$image['title']); ?>
                    <? if($label_title) { ?>
                    
                    	<span class='slideshow-label' style="background-color:rgba(<? print $sc[0].",".$sc[1].",".$sc[2]; ?>,0.6);">
                  		
                        <h2><? print $label_title; ?></h2>
						<span class="slideshow-label-text"><? print $label; ?></span>
                    	
                        
                        </span><? } ?>
				</div>	
        	
			<? 
			$index++;
				} //if image ends here
      		} //foreach ends here
	  	?>
        
        
        
		</div>
	
    
   <? /* <div id='slideshow-prev' class='slideshow-arrows' ></div>
	<div id='slideshow-next'  class='slideshow-arrows' ></div> */ ?>
    
     
	<div class='slideshow-nav-container'><div id='slideshow-nav'></div></div>
	
    
    </div>
    
    <div id="slideshow-strip-hide"></div>
   
            
            
	
	
	
	

	