<?
/*----------------------------------------------------------------------------------------

	Name:	general-page-header.tpl.php
	
	Description: Custom Template
	
	Function: Renders a header for a general page
	
	$variables : stores the data
	
----------------------------------------------------------------------------------------*/	
	

	// get given variables
	global $base_url;

	$image_path = $variables['image_path'];
	$title = $variables['title'];
	$byline = $variables['byline'];
	

	
?>
	<? /*-----HEADER ----------------------------------------------*/ ?>
    <div class="slideshow-back-strip"></div>
	<div class='header-container'>
    	
		<div class="header-image">
       
        	<?php  print theme_image($image_path,$title,$title); ?>
        </div>
        <? /*
        <div class="header-text">
        	<span class="header-title"><? print $title; ?></span>
            <? print " - "; ?>
			<span class="header-byline"><? print $byline; ?></span>            
        </div>    
		*/ ?>
	</div>
    
    
	
	
	
	
	

	