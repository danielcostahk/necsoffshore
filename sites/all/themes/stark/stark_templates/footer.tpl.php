<?
/*----------------------------------------------------------------------------------------

	Name:	footer.tpl.php
	
	Description: Template for bottom footer
	
	Function: Displays the bottom foter
	
	$variables : stores the data
	
----------------------------------------------------------------------------------------*/	
	

	// get given variables
	global $base_url;
	global $user;
	$footer_node = $variables[0];
	
	$about_text = $footer_node->body;
	$contact_url = url('contact');
	$linkedin = "http://www.linkedin.com";
	$viadeo = "http://www.viadeo.com";
	
	$terms = url('terms');
	$privacy = url('privacy');
	$disclaimer = url('disclaimer');
	//$privacy_url = url('privacy');
	
	
	$company = url('About');
	$services = url('Services');
	$jobposts = url('JobPosts');

	?>
	
    <? /*
	
		*/ ?>
    
    <div id="footer-network">
    	<div id="network" class="float-box">
        	<div>
        	<span class="menu-title">Join Our Network</span>
        	<span>
        	<a href="<? print $linkedin; ?>"><span id="linkedin" title="Linkedin" class="contact"></span></a>
             <a href="<? print $viadeo; ?>"><span id="viadeo" title="Viadeo" class="contact"></span></a>
        	</span>
            </div>
        </div>
        
        <div id="site-map" class="float-box">
        	<span class="menu-title">Site Map</span>
        	<ul>
            	<li><a href="<?php print url('<front>'); ?>">Home</a></li>
                <li><a href="<?php  print $company; ?>">Company</a></li>
                <li><a href="<?php  print $services; ?>">Services</a></li>
                <li><a href="<?php print $jobposts; ?>">Recruitment</a></li>
           </ul>     
        </div>
        
        <div id="members" class="float-box">
        	<span class="menu-title">Members Area</span>
            <ul>
            	
            	<? if(IsUser('unregistered')) { ?>
                <li><a href="<? print url('user'); ?>"> <? print t('Login'); ?></a></li><? } ?>
                <? /* if(IsUser('internal')) { 
                <li><a href="<? print url('admin/'); ?>"><? print t('Intranet'); ?></a></li>
                 */ ?>

                 <?  if(IsUser('internal')) {  ?>
                <li><a href="<? print url('forum'); ?>"><? print t('Forum'); ?> </a></li>
                <li><a href="<? print url('files'); ?>"><? print t('Files'); ?> </a></li>
                <? } //if is role user ?>
                <? if(IsUser('registered')) { ?>
                <li><a href="<? print url('user'); ?>"> <? print t('Personal Area');?></a></li>
                <? } ?>
            </ul>    
        </div>
        
        <div id="company-contact" class="float-box">
        	<span class="menu-title">Contact Us</span>
            <div id="address">
            <div class="float">
            <p>1 Raffles Place - #44-02, One Raffles Place Tower One</p>
           	<p>Singapore 048616</p>
            </div>
            <div class='address-mail'>
            <p>biz@necsoffshore.com</p>
            </div>
            
            </div>
            <?php /*
            <div id="phone">
            	<div><span class="subtitle">Singapore (fix)</span> +65 3157 8891</div>
                <div><span class="subtitle">(mobile)</span> +65 9665 7920</div>
                <div><span class="subtitle">Korea (mobile)</span> +82 10 4946 6590</div>
                <div><span class="subtitle">China (mobile) </span>+86 137 74346796</div>
             	<div><span class="subtitle">Fax </span> +65 3157 6478</div>
            </div>
            */ ?>
        </div>
        
      <? /*
        
    	<div class="left-network">
        	<span class="title"><? print t('Join our Network'); ?><span class="byline"><? print t(':: We are just a click away'); ?></span></span>
        	
           
            <span id="email" class="contact"><a href="mailto:info@necsoffshore.com">biz@necsoffshore.com</a></span>
            <span id="phone" class="contact">
            	<span>Singapore (fix) +65 3157 8891 | (mobile) +65 9665 7920</span>
                <span>Korea (mobile) +82 10 4946 6590</span>
                <span>China (mobile) +86 137 74346796</span>
                <span>Fax  +65 3157 6478</span>
             </span>
				       		
        </div>
        
        <div class="right-network">
        	<span class="title">Contact</span>
            <p>Do you have any questions or you just wish to
            say hi? Please go our contact page and leave us a note!
            </p>
            <span class="contact-icon"><a href="<? print $contact_url; ?>">Contact Form</a></span>
        </div>
    
      */ ?>
    
    
    </div>
	
    
    
    <div id="footer-bottom">

		<div class="rights">
        	 <span><a href="<? print $terms; ?>">Terms of Use</a></span>|
            <span><a href="<? print $privacy; ?>">Privacy Policy</a></span>|
            <span><a href="<? print $disclaimer; ?>">Disclaimer</a></span>
        </div>
		<div class="rights">
        	<span>Necs @All rights reserved 2013</span>
         </div>
        

	</div>
	

