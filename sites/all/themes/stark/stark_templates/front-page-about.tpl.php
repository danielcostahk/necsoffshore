<?
/*----------------------------------------------------------------------------------------

	Name:	frontpage-slideshow.tpl.php
	
	Description: Custom Template
	
	Function: Render html given a specific data
	
	$variables : stores the data
	
----------------------------------------------------------------------------------------*/	
	
	global $language;
	
	$language_index =0;
	if($language->language=='zh-hans') $language_index=1;

	// get given variables
	$frontpage_node = $variables[0];
	$index=0;
	$intro_title = $frontpage_node->field_frontpage_introtext_title[$language_index]['value'];
	$intro_text=$frontpage_node->field_frontpage_introtext[$language_index]['value'];
	$company_url = url('About');
	
?>

    <? /*-----MAIN Text ----------------------------------------------*/ 
		$bid_url = url('Invite-bid');
		$contact_url = url('Contact');
	?>
    <div class='main-content'>
    
    		<div id="about-content" class='main-text'>
              
                <h2><? print $intro_title; ?></h2>  <? /*<span class="title-icon"></span> */ ?>
               	<div class="main-text-container">
				<? print $intro_text; ?>
                	 <span id="about-link" class="main-text-link"><a href="<? print $company_url; ?>"><? print t('More >'); ?></a></span>
               	</div>
			</div>
   
   			
		   <? /*-----Bid----------------------------------------------*/ ?>
           <div id="bid-content" class="main-text">
                
                    
                        <h2><?php print t('Bid'); ?></h2>
                        <div class="main-text-container">
                        Please feel free to fill the following invitation to bid. We will make sure to maintain the security and confidentiality of any client trusted information.
						<span id="bid-link" class="main-text-link"><a href="<? print $bid_url; ?> " rel="shadowbox;min-width=662px;min-height=785px;"><? print t('Invitation to bid >'); ?></a></span>

                        Or you can contact us directly by using the contact form for general inquiry.
                <span id="contact-link" class="main-text-link"><a href="<? print $contact_url; ?> " rel="shadowbox;min-width=662;min-height=520px;"><? print t('Contact Form >'); ?></a></span>
                        <div class="main-text-container">
                        
             
           
           
           </div>
   
   </div>
            
            
	
	
	
	

	