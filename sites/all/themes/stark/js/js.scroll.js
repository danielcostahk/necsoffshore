// Variable to hold scroll type
var slideDrag,
// Width of .scroll-content ul
slideWidth = 330,
// Speed of animation in ms
slideSpeed = 400;
 
// Initialize sliders
$(".scroll-slider").slider({
    animate: slideSpeed,
    start: checkType,
    slide: doSlide,
    max: slideWidth 
});
 
 
function checkType(e){
    slideDrag = $(e.originalEvent.target).hasClass("ui-slider-handle");
}
 
function doSlide(e, ui){
    var target = $(e.target).prev(".scroll-content"),
    // If sliders were above the content instead of below, we'd use:
    // target = $(e.target).next(".scroll-content")
    maxScroll = target.attr("scrollWidth") - target.width();
 
    // Was it a click or drag?
    if (slideDrag == true){
        // User dragged slider head, match position
        target.attr({scrollLeft: ui.value * (maxScroll / slideWidth) });
    }
    else{
        // User clicked on slider itself, animate to position
        target.stop().animate({
            scrollLeft: ui.value * (maxScroll / slideWidth)
        }, slideSpeed);
    }
}