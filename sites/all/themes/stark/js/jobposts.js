/*
 *	Name: jobposts.js 
 * 
 *  Description: js for user file 
 * 
 * 
 */


$(document.body).ready(function () {



	$(".jobpost-content .jobpost-row .more-info").click(function(){
																 
		extra_info = $(this).parent().parent().find('.extra-info');
		if(extra_info.css('display')=='none') {
				extra_info.slideDown('fast'); 
				$(this).addClass('open-arrow');
				//$(this).css('background-position','40px -30px');
		}
		else {
			extra_info.slideUp('fast');
			$(this).removeClass('open-arrow');
			//$(this).css('background-position','top right');
		}
	});	


	// ajax request
	$(".jobpost-content .jobpost-row .job-application").click(function(){ 
		
		// if its a valid applied application
		status = $(this).attr('status');
		
	
		if(status==false||status=='false'|| status==null) {  } //invalid job application
		
		else { //the user has not applied yet

		
		/*---------------------------
			Fetching base url needs review
			should be store in drupal global ajax variables
		-----------------------------------------------*/		
		pathArray = window.location.pathname.split( '/' );
		host = pathArray[1];
		if(host=='stark') base_url = 'http://'+window.location.hostname+'/'+pathArray[1]+'/';
		else  base_url = 'http://'+window.location.hostname+'/Necs/website/';

		console.log('base url');
		console.log(base_url);
		$(this).attr('status','false'); //prevent from another ajax request being called again
		application = $(this);

					$.ajax({
					  type:'POST',
					  //type: 'GET',	
					  url: base_url+'jobpost/register/user',
					  data: { jobid: application.attr('id') },
					  beforeSend:function(){ 			
						//nice to see how to do with Drupal Settings
						application.html('<img src="'+base_url+'sites/all/themes/stark/stark_images/loading_icon.gif" alt="loading.."/>');
					  },
					  success: function(data) {
				
							var content = eval(data);
							application.fadeOut('fast',function() { application.html(content[0]).fadeIn('slow') });
					  }  
					  ,
					   error:function(){
						   console.log(error);
						  warning = "Some error ocurred, please contact the webmaster for further details";
						  if(application)
							application.fadeOut('fast',function() { application.html('<span title="'+warning+'">Error</span>').fadeIn('slow') });
						}
					 
					});  //ajax request ends here
		
		
		
		} //status ends here
		
		
	});	

		

}); // document ready ends here


