<?php
/**
 * @file
 * Main view template
 *
 * Variables available:
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - view
 *   - view-[name]
 *   - view-id-[name]
 *   - view-display-id-[display id]
 *   - view-dom-id-[dom id]
 * - $css_name: A css-safe version of the view name.
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  
  <?php if ($header): ?>
    <div class="view-header">
      <?php
		foreach($header as $header_row) {
			if($header_row!='term'&&$header_row!='About')print '<div id="header-'.$header_row.'"><span>'.$header_row.'</span></div>';
		}
	 
	  ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>


  <?php if ($rows): ?>
  	
  <? /*  main chunk ---------------------------------*/ ?>
    <div class="view-content">
    	
    	
        
      	<div class="jobpost-content">
		  <?php 
		  global $user;
		  global $base_url;
		  
		  if($user->uid) $is_user = true;
		  else $is_user = false;
		  
		  $job_data = jobpost_get_user_data($user->uid);
		 
		  if(intval(arg(1))==NULL) {
		  	
			$info_style = 'display:none';
		  }
		  else $arrow_style = 'open-arrow';
		 
			//print_r($rows);
			
            foreach($rows as $row) {
				//$terms = taxonomy_node_get_terms_by_vocabulary(node_load($row['nid']),2);
				//print_r($terms);
				/*foreach($terms as $term){
					print  '<span id="'.$term->tid.'" class="jobpost-type">'.$term->name.'</span>';
					
				}*/

				print '<div id="jobpost-'.$row['nid'].'" class="jobpost-row">';
            	print '<div class="row-1">';
					print '<div class="id-row">'.$row['nid'].'</div>';
					print '<div class="title-row">'.$row['title'].'</div>';
					//print '<div>'.substr($row['body'],0,20).'..</div>';
					print '<div>'.$row['field_jobpost_experience_value'].'</div>';
					 /* print '<div>'.$row['field_jobpost_expiration_value'].'</div>'; */
					print '<div>'.$row['field_jobpost_location_value'].'</div>';
					print '<div class="jobpost-type" id="term-'.$row['tid'].'"><span class="jobtype-info-block" >'.$row['name'].'</span></div>';
					
					/*-- apply tag has some checks --*/
					if($is_user)  $apply_tag = jobpost_apply_tag($user->uid,$row['nid'],$job_data);
					else  $apply_tag = job_post_inactive_tag_html();
					print $apply_tag;
					/*----------------------------------*/
					
					print job_post_email_share($row['nid'],$row['title'],'recruit');
					
					//'<div class="job-share"><span><a href="mailto:?subject='.$email_subject.'&body='.$email_body.'">share</a><span></div>';
					
					
					print '<div class="more-info '.$arrow_style.'"><span></span></div>';
				print '</div>';
				
				
             	print '<div class="extra-info" style="'.$info_style.'">'.$row['body'].'</div>';
				//print '<div class="extra-info" style="display:none;">Hellohello</div>';
				
				
				print '</div>';
            }
          
           ?>
       </div>
       
    </div>
 
  <? /*  chunk ends here ---------------------------------*/ ?>
  
  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>


 
  <?php endif; ?>

</div><!-- /.view -->
