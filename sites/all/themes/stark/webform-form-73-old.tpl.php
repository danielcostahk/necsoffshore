<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 */
?>
<?php
  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  

   //print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above.
  
 	// print_r($form);
 
 	// get extra info
	$form_node = node_load(74);
	
	$banner_filepath = $form_node->field_gp_banner_image[0]['filepath'];
	$banner_title =  $form_node->field_gp_banner_image[0]['title'];
	$main_text=  $form_node->field_gp_maintext[0]['value'];

  ?>	
 	<div class="contact-container">
    <? /*
 	<div class="banner-image">
	<? print theme_image($banner_filepath,$banner_title,$banner_title); ?>
	</div>
	*/  ?>
    
    <div class="contact-content">
    <span id="contact-separator" class="separator"></span>
        <div id="upper-side">
        <? print $main_text; ?>
        </div>
    </div>

	<div id="bottom-side">
	<?  print drupal_render($form); ?>
	</div>
    </div>

    </div> <!-- main contact banner ends here --> 
 <? 
  
