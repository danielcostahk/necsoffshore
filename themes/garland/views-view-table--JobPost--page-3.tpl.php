<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * @ingroup views_templates
 */
?>
<table class="<?php print $class; ?>"<?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th class="views-field views-field-<?php print $fields[$field]; ?>">
          <?php print $label; ?>
          
        </th>
      <?php endforeach; ?>
      	  <th class="open-application views-field views-field-<?php print $fields[$field]; ?>">
          <?php print "Applications" ?>
          
        </th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $count => $row): 
		$row_id = $row['nid'];
		?>
      <tr class="<?php print implode(' ', $row_classes[$count]); ?>" style="position:relative;">
      
      	
        <?php foreach ($row as $field => $content): ?> 
          <td class="views-field views-field-<?php print $fields[$field]; ?>">
            <?php print $content; ?>
         	
          </td>
        <?php endforeach; ?>
        
        <? 
				// needs improvement take out user roles besides normal users
			$user_string =NULL;
			$users_result = db_query(
			db_rewrite_sql("SELECT ctj.field_user_jobpost_rel_uid_value, ctj.field_user_jobpost_rel_jobid_value
							FROM node AS n INNER JOIN content_type_jobpost_user_rel as ctj  WHERE n.nid=ctj.nid AND
							 ctj.field_user_jobpost_rel_jobid_value=".$row['nid']));
						
			while ($users_row = db_fetch_object($users_result)) 
				$user_string .= (string)$users_row->field_user_jobpost_rel_uid_value.",";
			
        
        ?>
        
        
        <td id="<? print $row_id; ?>" class="application-link views-field views-field-<?php print $fields[$field]; ?>" style="cursor:pointer;text-decoration:underline;">
           <?php if($user_string!=NULL) { print "View";  } ?>
		</td>
	</tr>
      
   
	  <? /*------------------------change chunk ---------*/?>
      
   

        	<? 
			
			if($user_string!=NULL) {
				print "<tr id='applications-".$row_id."' class='user-profile' style='display:none;'><td></td><td>";
				$users_view = views_get_view('Users');
				$user_string = substr($user_string,0,strlen($user_string)-1);
				$user_arguments = array($user_string);
				$users_view->set_arguments($user_arguments);  
				print $users_view->render('page_3');
			
				print "</td></tr>";
			}
		?>
         
        <? /*------chunk ends here------*/ ?>
      
   
    <?php endforeach; ?>
  </tbody>
</table>

<script language="javascript" type="text/javascript">
$(document.body).ready(function () {
	
		
		/*-----------------------------------------------------------
			JobPost
		--------------------------------------------------------*/	
		$(".application-link").click(function(){
	
			
			//alert($(this).attr('id'));
			var id = "#applications-"+$(this).attr('id');
			
			if($(id).css('display')=='none')
				$(id).slideDown('slow'); 
			else $(id).slideUp('slow'); 
		
		});	
		
		
		
		
		
		
		
		
		
			
}); // document ready ends here

</script>

